window.addEventListener("load", function() {
  initEventListeners();
  loadContent();
});

/**
 * Initializes event listeners for popup.html
 */
function initEventListeners() {
  document.getElementById("showChannels").addEventListener("click", showChannels);
  document.getElementById("showVideos").addEventListener("click", showVideos);
}

/**
 * Render initial content
 */
function loadContent() {
  updateMonitoredChannelsContent();
  updateRenderVideosContent();
  showVideos()
}

/**
 * Shows the channels view
 */
function showChannels() {
  document.getElementById("videos").style.display = "none";
  document.getElementById("channels").style.display = "block";
}

/**
 * Shows the videos view
 */
function showVideos() {
  document.getElementById("channels").style.display = "none";
  document.getElementById("videos").style.display = "block";
}

/**
 * Sets "monitoredChannels" div content
 */
function updateMonitoredChannelsContent() {
  var channels = getChannels();
  document.getElementById("channels").innerHTML = makeList(channels);
}

/**
 * Sets "newVideos" div content
 */
function updateRenderVideosContent() {
  var videos = getVideos();
  document.getElementById("videos").innerHTML = makeList(videos);
}

/**
 * Constructs an html <ul> from array
 * @param content {[string]} html representation of list items
 * @return {string} html <ul>
 */
function makeList(content) {
  var tag = "<ul>";
  for(var c in content) {
    tag += "<li>" + content[c] + "</li>";
  }
  tag += "</ul>";
  return tag
}