/**
 * Returns a list of channels that are monitored
 * @return {[string]} A list of monitored channel names
 */
function getChannels() {
  return ["testChannel1", "testChannel2"];
}

function getVideos() {
  return ["testVideo1", "testVideo2"];
}


/**
 * Adds a channel to the list of monitored channels
 * @param channel A channel name
 */
function addMonitoredChannel(channel) {

}

/**
 * Gets the video count for a given channel
 * @param channel the name of the channel
 * return {int} The number of videos on the channel
 */
function videoCountForChannel(channel) {

}

/**
 * Saves the monitored channels to file
 */
function saveMonitoredChannels() {

}

/**
 * Loads monitored channels from file
 * @return {[{string, uploads}]} A list of channel names and their video upload counts
 */
function loadMonitoredChannels() {

}